/** React */
import React, { useState, useEffect } from 'react'
import { View, StyleSheet, ActivityIndicator } from 'react-native'
import AsyncStorage from '@react-native-async-storage/async-storage'

/** Expo */
import * as Location from 'expo-location'
import * as Device from 'expo-device';
import * as Battery from 'expo-battery';

/** UI */
import { Button, Input, Text } from 'react-native-elements'
import { SafeAreaProvider } from 'react-native-safe-area-context'

/** Service */
import deviceService from './src/service/deviceService'

export default function App() {
    const [location, setLocation] = useState(null)
    const [errorMsg, setErrorMsg] = useState(null)
    const [isRegistered, setIsRegistered] = useState(false)
    const [deviceId, setDeviceId] = useState(null)
    const [isWaitingForCode, setIsWaitingForCode] = useState(false)
    const [registerCode, setRegisterCode] = useState('')
    const [isLoading, setIsLoading] = useState(false)
    const [deviceStatus, setDeviceStatus] = useState(true)
    const [userTakenDevice, setUserTakenDevice] = useState(null)

    useEffect(() => {
        ;(async () => {
            setIsLoading(true)
            let { status } = await Location.requestForegroundPermissionsAsync()
            if (status !== 'granted') {
                setErrorMsg('Permission to access location was denied')
                return
            }

            let location = await Location.getCurrentPositionAsync({})
            setLocation(location)

            try {
                const value = await AsyncStorage.getItem('@is_registered')
                if (value !== null && value) {
                    setIsRegistered(true)
                } else {
                    setIsRegistered(false)
                }
            } catch (e) {
                // error reading value
            } finally {
                setIsLoading(false)
            }
        })()
    }, [])

    useEffect(() => {
        if (isRegistered) {
            ;(async () => {
                try {
                    setIsLoading(true)

                    const value = await AsyncStorage.getItem('@device_id')
                    if (value !== null && value) {
                        setDeviceId(value)
                    }
                } catch (e) {
                    // error reading value
                } finally {
                    setIsLoading(false)
                }
            })()
        }
    }, [isRegistered])

    useEffect(() => {
        if (deviceId && location) {
            if (Device.isDevice) {
                sendInfo()
            }
        }
    }, [deviceId, location])

    const sendInfo = async () => {
        try {
            setIsLoading(true)

            const batteryLevel = await Battery.getBatteryLevelAsync()
            
            await deviceService.sendDeviceInfo(
                deviceId,
                Device.brand,
                Device.modelName,
                Device.osName,
                [location['coords']['longitude'], location['coords']['latitude']],
                batteryLevel,
                location['coords']['altitude']
            )
        } catch (err) {
            console.error(err)
        } finally {
            setIsLoading(false)
        }
    }

    useEffect(() => {
        const interval = setInterval(async () => {
            if (deviceId) {
                try {
                    setIsLoading(true)
                    const response = await deviceService.getDeviceInfo(deviceId)
                    console.log(response)
                    if (response.status === 'ok') {
                        setDeviceStatus(response.payload.isAvailable)

                        setUserTakenDevice(response.payload.userNameTakenDevice)
                    }
                } catch(err) {
                    console.error(err)
                } finally {
                    setIsLoading(false)
                }
            }
        }, 10000)
      
        return () => clearInterval(interval)
      }, [deviceId])

    const registerDevice = async (deviceId) => {
        try {
            await AsyncStorage.setItem('@is_registered', "true")
            await AsyncStorage.setItem('@device_id', deviceId)
        } catch (err) {
            // saving error
        }
    }

    const onSendRegisterCode = async () => {
        try {
            setIsLoading(true)
            setIsWaitingForCode(!isWaitingForCode)

            const response = await deviceService.verifyDevice(registerCode)

            if (response.status === 'ok' && response.payload.result) {
                await registerDevice(response.payload.deviceId)
                setDeviceId(response.payload.deviceId)
            }
        } catch (err) {
            console.error(err)
        } finally {
            setIsLoading(false)
        }
    }

    return (
        <SafeAreaProvider>
            <View style={styles.container}>
                {!isRegistered && !isWaitingForCode && !isLoading && (
                    <Button
                        raised
                        title="Зарегистрировать устройство"
                        onPress={() => setIsWaitingForCode(!isWaitingForCode)}
                    />
                )}
                {isWaitingForCode && !isLoading && (
                    <>
                        <Input
                            placeholder="Код"
                            keyboardType="numeric"
                            onChangeText={(value) => setRegisterCode(value)}
                        />
                        <View>
                            <Button
                                disabled={registerCode.length < 4}
                                title="Отправить"
                                onPress={onSendRegisterCode}
                            />
                            <Button
                                title="Отменить"
                                type="outline"
                                style={styles.declineButton}
                                onPress={() =>
                                    setIsWaitingForCode(!isWaitingForCode)
                                }
                            />
                        </View>
                    </>
                )}
                {isLoading && <ActivityIndicator size="large" color="#0000ff" />}
                {isRegistered && !isLoading && <Text h4>Статус устройства: {deviceStatus ? 'свободно' : 'занято'}</Text>}
                {isRegistered && !isLoading && userTakenDevice && <Text h4>Занято пользователем: {userTakenDevice}</Text>}
            </View>
        </SafeAreaProvider>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 32,
    },
    declineButton: {
        paddingTop: 16,
    },
})
