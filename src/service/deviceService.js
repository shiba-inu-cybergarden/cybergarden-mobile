/** Config */
import { baseUrl } from '../config'

class DeviceService {
    async verifyDevice(pinCode) {
        const url = `${baseUrl}/verifyAuthCode`
        const response = await fetch(url, {
            method: 'post',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                pinCode,
            }),
        })

        return await response.json()
    }

    async sendDeviceInfo(
        deviceId,
        deviceBrandName,
        deviceModel,
        deviceOS,
        coordinates,
        batteryCharge,
        height
    ) {
        const url = `${baseUrl}/setDeviceDynamicProperty`
        const response = await fetch(url, {
            method: 'post',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                deviceId,
                deviceBrandName,
                deviceModel,
                deviceOS,
                coordinates,
                batteryCharge,
                height,
            }),
        })

        return await response.json()
    }

    async getDeviceInfo(deviceId) {
        const url = `${baseUrl}/getDevice?deviceId=${deviceId}`
        const response = await fetch(url, {
            method: 'get'
        })

        return await response.json()
    }

    async getUserName(userId) {
        const url = `${baseUrl}/getUserName?userId=${userId}`
        const response = await fetch(url, {
            method: 'get'
        })

        return await response.json()
    }
}

export default new DeviceService()
